---

title: Shadowing the CEO of GitLab
date: "2024-01-09T10:00:12.695Z"
template: "post"
draft: false
slug: "/posts/ceo-shadow-learning/"
category: "CEO Shadow"
tags:
  - "CEO Shadow"
description: "A closer look into the day-to-day responsibilities of a CEO of a publicly traded all-remote company."
socialImage: "./media/desk.jpg"
---
# Background

Joining GitLab as a young professional, I embarked on an enriching three-year journey with the [Digital Experience team at GitLab](https://handbook.gitlab.com/handbook/marketing/digital-experience/). GitLab, a software technology company renowned for its all-remote and open-source ethos from inception, has pioneered in [educating others about remote work](https://www.coursera.org/learn/remote-team-management). The company's culture, anchored by a comprehensive handbook, is the definitive guide for everything from core values to everyday operational strategies. This documentation-first approach underpins our transparent, asynchronous-first culture.

My venture into this program started with something my manager([who is herself an alumni of the program!](https://about.gitlab.com/blog/2021/02/26/ceo-shadow-takeaways-from-barker/)) encouraged me to inquire. The CEO shadow program has been on hiatus for some time, so I wanted to sign up for this program and take advantage of this opportunity.
My motivation stems from wanting to understand the company from ten thousand feet up in the air. Based on my role, I want to contribute to the business in the best way I can.

## Goals for the program:

1. **Understanding Company Values at the Executive Level**: I aim to gain new insights into our values, seeking a unique interpretation that differed from the conventional understanding.
1. **Enhancing Assertiveness**: I want to voice my opinions more confidently. Historically reserved, I was eager to embrace a more vocal role.
1. **Crafting a Personal Mission/Vision Statement**: With my tenure at GitLab, I want to refine my personal mission and vision, intending to finalize it by the program's end.


# Pre-Program Thoughts

I've always valued deep intrinsic motivation, pondering extensively on my tasks and their underlying reasons. The interplay between our values and potential anti-values intrigued me, leading me to contemplate how values that have room for nuance manifest in practice. I am interested in the scalability of GitLab's culture and the natural evolution of values. GitLab must I believe in an auto-corrective approach to maintain its core identity despite changing conditions.

Reflecting on seniority, I recognized its inevitable influence on value perception and implementation, emphasizing the need for awareness and corrective measures.

# Learnings throughout the program

* **Note-Taking Challenges**: Mastering effective note-taking, especially in large meetings, was a significant hurdle. I learned the importance of collaborative documentation and appreciated the skill of our Executive Business Assistants (EBAs) in this area.
* **Direct Exposure to Leadership**: The program's immersive nature, throwing participants directly into high-level meetings, was both challenging and rewarding. Interacting with the E-group and witnessing their commitment to our values was enlightening.
* **Agenda Preparation and Documentation**: I observed and learned the importance of proactive agenda setting and maintaining document hygiene.
* **Merge Requests (MRs) for Documentation**: I embraced MR-first. If things looked out of place, it probably is. Starting with an MR allows for efficient information sharing.
* **Personal Interactions with Leadership**: Engaging with Sid and other executives was a highlight, offering insights into their perspectives and leadership styles. Stella (Chief of Staff) was a tremendous support and advocate for the program.
* **Broader Industry Perspectives**: My discussions with various people throuough the company were enlightening, covering topics from the evolution of our handbook approach to her intriguing AI projects.

# Reflection After the Program

The CEO shadow program at GitLab was an invaluable experience, offering a unique glimpse into executive decision-making and company values in action. The dedication and skill of the EBAs was impressive, highlighting their crucial (underappreciated as an outsider) role in the executive team's efficiency. Stella's support throughout the program was remarkable. The program's structure, minimally impacting the CEO's schedule, is a testament to its thoughtful design. I want to take a second to thank Yin and Ben for being excellent co-shadows througout my tenure with the program. Finally, I want to thank Sid for opening up his meetings and allowing me to share space throughout my tenure.

## Achievements Against My Goals

1. **Understanding Company Values**: Witnessing executives, including VPs and Directors, actively embodying our values was inspiring. The experience reinforced my belief in our handbook as a dynamic, living document.
1. **Enhancing Assertiveness**: The program organically fostered my confidence in speaking up. Familiarity with leadership demystified their roles, making them more approachable.
1. **Personal Mission/Vision Statement**: The program prompted me to consider a deeper investment in data and statistics, potentially exploring more education while weighing the opportunity costs. My interdisciplinary background also presents exciting avenues for future exploration.

In conclusion, the CEO shadow program at GitLab was not just a professional milestone but a personal journey of growth, learning, and broadened perspectives.
