---

title: Design Philosphy impacted by Virgil Abloh
date: "2021-12-02T10:00:12.695Z"
template: "post"
draft: false
slug: "/posts/virgil/"
category: "Design Philosophy"
tags:
  - "Mentors"
description: "Nuggets from Virgil's Harvard talk. Core Studio Public Lecture: Virgil Abloh, “Insert Complicated Title Here”"
socialImage: "./media/notebook.jpeg"
---

> You have to sort of connect with some body of work or someone who formulated a thought and an aesthetic and build yours upon them. What most people won’t tell you is that the people that you look up to didn’t invent it themselves.

*Virgil Abloh, [30:48 of his Harvard Design School talk on youtube](https://youtu.be/qie5VITX6eQ?t=1848)*


I wanted to drop some impactful comments from that talk here. As a young man based out of California, I felt connected to his work with sneaker culture. I thought a lot of his perspective can be applied to my own work as a young professional.

## Some of that Ethos

Ideas and inspiration are self-referencing.

> A house is a machine to live in —Le Corbusier

Everything in a home has an intended purpose. Everything that we do and say should also have an intended purpose.

> 3%

Changing things 3% at a time. Take some ethos, understand it, and adapt it in some new way. I really like [Mark Ronson's TED talk](https://www.youtube.com/watch?v=H3TF-hI7zKc) about sampling as an extension of that philosophy.

> Design is assumed to work

People really take notice to when something that is assumed to work doesn't.
